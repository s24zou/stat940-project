import os

source_paths=["./sat/"]
suffix=".cnf"
output_paths=["./sat-clean/"]
for p in output_paths:
    os.makedirs(p, exist_ok=True)
for i in range(len(source_paths)):
    cur_s = source_paths[i]
    cur_o = output_paths[i]
    files = [cur_s+x for x in os.listdir(cur_s)]
    name_length=len(str(len(files)))
    for j in range(len(files)):
        with open(files[j],'r') as fp:
           content = fp.readlines() 
        content = content[7:-3]
        with open(cur_o+str(j+1).rjust(name_length,'0')+suffix,'w') as fp:
            fp.writelines(content)
print("Data Processing Done!")
