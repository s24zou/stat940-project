import random
import os

def generate_CNF_instance():
    num_vars = 20
    num_clauses = 91
    variables = [i for i in range(1, num_vars + 1)]
    clauses = []
    for i in range(num_clauses):
        clause = []
        for j in range(3):
            variable = random.choice(variables)
            if random.random() < 0.5:
                variable *= -1
            clause.append(variable)
        clauses.append(clause)
    return clauses

out_folder = "random-instance"
for n in range(500):
    cnf_instance = generate_CNF_instance()
    clauses = []
    heading = "p cnf 20 91 \n"
    clauses.append(heading)

    for clause in cnf_instance:
        clause = " ".join(str(literal) for literal in clause) + " 0 \n"
        clauses.append(clause)
    os.makedirs(out_folder, exist_ok=True)
    with open(os.path.join(out_folder, str(n) + ".cnf"), 'w') as fp:
        fp.writelines(clauses)