import networkx as nx
import os
import random

def idx_helper(input, bound):
    if input > bound:
        return bound-input
    return input

folder = "./graph-output-500"
out_folder = "./cnf-output-500"
for n in os.listdir(folder):
    cur_path = os.path.join(folder, n)
    cur_graph = nx.read_edgelist(cur_path)
    num_nodes = sorted([int(x) for x in list(cur_graph.nodes())])
    num_nodes = max(num_nodes)
    if num_nodes % 2 != 0:
        num_nodes += 1
    num_nodes = num_nodes // 2
    cycles = list(sorted(nx.simple_cycles(cur_graph, length_bound=3)))
    cycles = list(filter(lambda x: len(x)==3, list(sorted(nx.simple_cycles(cur_graph, length_bound=3)))))
    for i in range(len(cycles)):
        cycles[i] = [int(x) for x in cycles[i]]
    cycles = list(sorted(cycles))
    clauses = []
    for c in cycles:
        cur_clause = [str(idx_helper(x+1, num_nodes)) for x in c]
        clauses.append(' '.join(cur_clause)+" 0\n")
    clauses = random.sample(clauses, 91)
    #randomly sample 91 clauses

    clauses.insert(0, f"p cnf {num_nodes} {len(clauses)}\n")
    os.makedirs(out_folder, exist_ok=True)
    with open(os.path.join(out_folder, n), 'w') as fp:
        fp.writelines(clauses)
